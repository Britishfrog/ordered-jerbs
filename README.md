This test was done for OnTheBeach.com

/* 

The programs goal is to take a string input of Jobs, each one with its own dependencies that allow us to prioritize them. Given a string such as ("a=> b=>c c=>f d=>a e=>b f=> "), the program should be able to figure out the right order for these jobs, while taking into account empty strings or cyclic dependency. 

*/

This program uses jUnit 4.0 for testing purposes.

FileManager.java: Allows the parsing and unparsing of strings that represent the jobs and their dependencys.

Job.java: class that describes the Job object, including name and dependencies. Also contains methods to compare them.

CyclicDependancyException.java: Unique exception class for cases of cyclic dependency

FileManagerTest.java: jUnit prefabricated test class, allows the testing of all relevant test cases including null strings, single element string and cyclic dependencies.