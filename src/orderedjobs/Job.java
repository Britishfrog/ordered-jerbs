/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package orderedjobs;

/**
 *
 * @author Catbug
 */
public class Job {

    private final String name;
    private Job dependency;

    Job(String name, Job dependency) {
        this.name = name;
        this.dependency = dependency;
    }

    public String getName() {
        return name;
    }

    public Job getDependency() {
        return dependency;
    }

    public void setDependency(Job dependency) {
        this.dependency = dependency;
    }

    @Override
    public String toString() {
        return name;
    }

    public String getFullDescription() {
        return name + " => " + dependency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Job job = (Job) o;

        if (name != null ? !name.equals(job.name) : job.name != null) {
            return false;
        }

        return true;
    }

    @Override
    public int hashCode() {
        return name != null ? name.hashCode() : 0;
    }
}
